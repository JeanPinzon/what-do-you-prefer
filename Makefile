setup:
	brew cask install chromedriver libxml2
	bundle install
	gem install nokogiri -- --use-system-libraries --with-xml2-include=$(brew --prefix libxml2)/include/libxml2

test:
	cucumber tests