When('I fill in {string} in the {string} field') do |inputText, fieldName|
  fill_in fieldName, with: inputText
end

When("I fill in {string} in the {string} field and hit enter") do |inputText, fieldName|
  fill_in fieldName, with: inputText
  find_field(fieldName).native.send_keys(:return)
end

When('I click on the {string} button') do |buttonName|
  click_on(buttonName)
end

When('I click on the especific button {string}') do |cssSelector|
  find(:css, cssSelector).click
end

When('I click on the alert') do
  page.driver.browser.switch_to.alert.accept
end

When('I wait for the loading to dismiss') do
  expect(page).not_to have_css('.js-loading')
end